package pers.zh.kotlin.practice.mapper

import org.apache.ibatis.annotations.Param
import java.sql.SQLException


interface AnnouncementMapper {
    /**
     * 公告列表分页查询
     * 创建人：赵辉
     * 创建时间：2017-01-04
     * @param uid
     * *
     * @return
     * *
     * @throws SQLException
     */
    @Throws(SQLException::class)
    abstract fun queryGongGaoPage(@Param("uid") uid: String, @Param("cnm") cnm: String, @Param("bt") bt: String, @Param("vdt") vdt: String): List<Map<String, Any>>


    /**
     * 通过Nid进行查询
     * 创建人：赵辉
     * 创建时间：20167-01-06
     * @param nid
     * *
     * @return
     * *
     * @throws SQLException
     */
    @Throws(SQLException::class)
    abstract fun queryGongGaoByNid(@Param("nid") nid: String): Map<String, Any>

    /**
     * 标记为已读
     * 创建人：赵辉
     * 创建时间：2017-01-06
     * @param nid
     * *
     * @param uid
     * *
     * @param rid
     * *
     * @param rip
     * *
     * @param rdt
     * *
     * @return
     * *
     * @throws SQLException
     */
    @Throws(SQLException::class)
    abstract fun updateRead(@Param("rid") rid: String, @Param("nid") nid: String, @Param("uid") uid: String, @Param("rdt") rdt: String, @Param("rip") rip: String): Int?


    /**
     * 查看是否已读公告
     * 创建人：赵辉
     * 创建时间：2017-01-06
     * @param uid
     * *
     * @return
     * *
     * @throws SQLException
     */
    @Throws(SQLException::class)
    abstract fun queryIfRead(@Param("uid") uid: String, @Param("nid") nid: String): Int?


    /**
     * 删除公告
     * 创建人：赵辉
     * 创建时间：2017-01-06
     * @param nid
     * *
     * @return
     * *
     * @throws SQLException
     */
    @Throws(SQLException::class)
    abstract fun updateDel(@Param("nid") nid: String, @Param("deluid") deluid: String, @Param("deldt") deldt: String, @Param("delip") delip: String): Int?

    /**
     * 发送公告
     * 创建人：赵辉
     * 创建时间：2017-02-17
     * @param inId
     * *
     * @return
     * *
     * @throws SQLException
     */
    @Throws(SQLException::class)
    abstract fun doSendMes(@Param("nid") nid: String, @Param("bt") bt: String, @Param("nr") nr: String, @Param("inId") inId: String, @Param("inDt") inDt: String, @Param("inIp") inIp: String, @Param("del") del: String): Int?

}