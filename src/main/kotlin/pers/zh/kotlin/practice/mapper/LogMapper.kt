package pers.zh.kotlin.practice.mapper

import org.apache.ibatis.annotations.Param


interface LogMapper {
    /**
     * 插入日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param uid
     * @return
     */
    @Throws(Exception::class)
    abstract fun addLog(@Param("id") id: String, @Param("type") type: String, @Param("nr") nr: String, @Param("uid") uid: String, @Param("dt") dt: String, @Param("ip") ip: String): Int?


    /**
     * 日志分页查询
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param type
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun selectPage(@Param("type") type: String, @Param("nm") nm: String, @Param("dt") dt: String, @Param("uid") uid: String): List<Map<String, Any>>

    /**
     * 通过id查询单个日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param id
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun selectLogById(@Param("id") id: String): List<Map<String, Any>>

    /**
     * 回显操作类型
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param uid
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun selectLx(@Param("uid") uid: String): List<Map<String, Any>>

}