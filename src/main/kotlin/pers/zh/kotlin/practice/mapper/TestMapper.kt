package pers.zh.kotlin.practice.mapper


interface TestMapper {
    /**
     * 分页查询测试
     * @return
     * *
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun queryAll(): List<Map<String, Any>>
}