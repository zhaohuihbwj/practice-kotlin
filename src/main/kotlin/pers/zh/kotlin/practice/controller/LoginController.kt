package pers.zh.kotlin.practice.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.util.Assert
import org.springframework.util.StringUtils
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import pers.zh.kotlin.practice.exception.UserLoginFailedException
import pers.zh.kotlin.practice.service.LoginService
import javax.servlet.http.HttpServletRequest

/**
 * 登陆 Controller
 * @author Zhao Hui
 */
@RestController
@RequestMapping("/login")
class LoginController {
    @Autowired
    private val loginService: LoginService? = null

    /**
     * 登陆Controller
     * @param user 登录名
     * @param pas  密码
     */
    @PostMapping("/login.index")
    fun login(user: String, pas: String, request: HttpServletRequest): Boolean? {
        Assert.isTrue(!StringUtils.isEmpty(user), "用户名为空")
        Assert.isTrue(!StringUtils.isEmpty(pas), "密码为空")

        return loginService!!.login(user, pas, request)
    }

    @ExceptionHandler(UserLoginFailedException::class)
    fun loginFailedHandle(ex: UserLoginFailedException): ResponseEntity<*> {
        return ResponseEntity<Any>(ex, HttpStatus.BAD_REQUEST)
    }


    //登出
    @PostMapping("/logout.index")
    fun logout(request: HttpServletRequest): Boolean {
        request.session.invalidate()
        return true
    }
}