package pers.zh.kotlin.practice.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import pers.zh.kotlin.practice.service.TongJiService
import javax.servlet.http.HttpServletRequest

@Controller
@RequestMapping("/tongJi")
class TongJiController {
    @Autowired
    @Qualifier("tongJiService")
    private val tongJiService: TongJiService? = null

    /**
     * 生日统计
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @return
     * @throws Exception
     */
    @RequestMapping("/queryBir.do")
    @ResponseBody
    @Throws(Exception::class)
    fun queryZgByYear(request: HttpServletRequest): Map<String, Any> {
        return tongJiService!!.queryBir(request)
    }


    /**
     * 性别统计
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @return
     * @throws Exception
     */
    @RequestMapping("/querySex.do")
    @ResponseBody
    @Throws(Exception::class)
    fun querySex(request: HttpServletRequest): Map<String, Any> {
        return tongJiService!!.querySex(request)
    }
}