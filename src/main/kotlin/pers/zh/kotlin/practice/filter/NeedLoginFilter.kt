package pers.zh.kotlin.practice.filter

import java.io.IOException
import javax.servlet.*
import javax.servlet.annotation.WebFilter
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@WebFilter(filterName = "needLoginFilter", urlPatterns = arrayOf("*.do", "/page/*"))
class NeedLoginFilter : Filter{
    @Throws(ServletException::class)
    override fun init(filterConfig: FilterConfig) {

    }

    @Throws(IOException::class, ServletException::class)
    override fun doFilter(request: ServletRequest, response: ServletResponse, chain: FilterChain) {
        val httpServletRequest = request as HttpServletRequest
        val session = httpServletRequest.getSession(false)
        if (session == null) {
            (response as HttpServletResponse).sendRedirect(request.contextPath + "/login.html")
            return
        }
        chain.doFilter(request, response)
    }

    override fun destroy() {

    }
}