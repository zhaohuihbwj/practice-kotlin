package pers.zh.kotlin.practice.util

import org.springframework.web.context.request.RequestContextHolder
import org.springframework.web.context.request.ServletRequestAttributes
import java.text.SimpleDateFormat
import java.util.*

object MyUtil {
    /**
     * 获取ip地址方法
     * @return
     */
    fun getIpAddr(): String? {
        val request = (RequestContextHolder.getRequestAttributes() as ServletRequestAttributes).request
        var ip: String? = request.getHeader("x-forwarded-for")
        if (ip == null || ip.length == 0 || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("Proxy-Client-IP")
        }
        if (ip == null || ip.length == 0 || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.getHeader("WL-Proxy-Client-IP")
        }
        if (ip == null || ip.length == 0 || "unknown".equals(ip, ignoreCase = true)) {
            ip = request.remoteAddr
        }
        return ip
    }


    /**
     * 获得GUID
     * @return
     */
    fun newGuid(): String {
        return UUID.randomUUID().toString().toUpperCase().replace("-", "")
    }


    /**
     * 获取当前时间
     * @return
     */
    fun getDatetime(): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        return sdf.format(Date())
    }

}