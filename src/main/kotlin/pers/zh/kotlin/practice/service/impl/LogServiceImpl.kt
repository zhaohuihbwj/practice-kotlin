package pers.zh.kotlin.practice.service.impl

import com.github.pagehelper.PageHelper
import com.github.pagehelper.PageInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pers.zh.kotlin.practice.exception.LogException
import pers.zh.kotlin.practice.exception.SqlException
import pers.zh.kotlin.practice.mapper.LogMapper
import pers.zh.kotlin.practice.service.LogService
import pers.zh.kotlin.practice.util.MyUtil
import java.util.HashMap
import javax.annotation.Resource
import javax.servlet.http.HttpServletRequest

@Service("logService")
class LogServiceImpl : LogService{
    @Autowired
    private val logMapper: LogMapper? = null

    @Resource
    private val request: HttpServletRequest? = null

    /*
     * 添加日志
	 * 创建人：赵辉
	 * 创建时间：2017-05-20
     * @param type
     * @param nr
     */
    @Transactional
    override fun addLog(type: String, nr: String): Boolean? {
        val uid = request!!.session.getAttribute("uid").toString()
        val ip = MyUtil.getIpAddr()
        val id = MyUtil.newGuid()
        val dt = MyUtil.getDatetime()

        try {
            return logMapper!!.addLog(id, type, nr, uid, dt, ip!!)!! > 0
        } catch (e: Exception) {
            throw SqlException("添加日志失败")
        }

    }

    /**
     * 查看日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param type 类型
     * @param nm 姓名
     * @param dt 时间
     * @param _page 页数
     * @param _pageSize 每页的大小
     * @return 当前页数据
     */
    @Throws(Exception::class)
    override fun queryLogPage(type: String, nm: String, dt: String, _page: String, _pageSize: String, uid: String, userLx: String): Map<String, Any> {
        var uid = uid

        // 定义返回值
        val result = HashMap<String, Any>()

        //如果类型为1管理员，则不传uid
        if (userLx == "1") {
            uid = ""
        }

        //分页
        PageHelper.startPage<Any>(Integer.valueOf(_page)!!, Integer.valueOf(_pageSize)!!)
        val mapList = logMapper!!.selectPage(type, nm, dt, uid)
        result.put("data", mapList)
        //总数
        val page = PageInfo<Map<String, Any>>(mapList)
        result.put("count", page.total)

        return result
    }

    /**
     * 通过日志id 查看一条日志
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param id 日志id
     * @return 此id对应的数据
     */
    override fun queryLogById(id: String): List<Map<String, Any>> {
        try {
            return logMapper!!.selectLogById(id)
        } catch (e: Exception) {
            throw LogException("读取失败")
        }

    }


    /**
     * 个人日志操作类型的回显
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param uid
     * @return
     */
    override fun queryMyLx(uid: String): List<Map<String, Any>> {
        try {
            return logMapper!!.selectLx(uid)
        } catch (e: Exception) {
            throw LogException("读取失败")
        }

    }

    /**
     * 所有日志操作类型的回显
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param userId
     * @param userLx
     * @return
     */
    override fun queryLx(userId: String, userLx: String): List<Map<String, Any>> {
        var uid = ""
        //如果类型为3管理员，则不传UID
        if ("3" != userLx) {
            uid = userId
        }
        try {
            return logMapper!!.selectLx(uid)
        } catch (e: Exception) {
            throw LogException("读取失败")
        }

    }
}