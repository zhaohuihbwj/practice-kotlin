package pers.zh.kotlin.practice.service

import javax.servlet.http.HttpServletRequest

interface UserService {
    //分页查询
    @Throws(Exception::class)
    abstract fun queryPage(douid: String, _page: String, _pageSize: String, sex: String, nm: String, type: String, del: String): Map<String, Any>

    //添加用户
    @Throws(Exception::class)
    abstract fun addUser(douid: String, user: String, pas: String, nm: String, sex: String, birthday: String, type: String, del: String): Map<String, Any>

    //通过UID查看用户的所有信息
    abstract fun queryUserByUid(uid: String): Map<String, Any>

    //修改用户信息
    @Throws(Exception::class)
    abstract fun doEditUserByUid(douid: String, uid: String, type: String, del: String): Map<String, Any>

    //查看用户名
    @Throws(Exception::class)
    abstract fun queryName(request: HttpServletRequest): Map<String, String>

    //查询生日信息
    @Throws(Exception::class)
    abstract fun queryBir(): Map<String, Any>


    /**
     * 重置用户密码
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param request
     * @param uid
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun doRePas(request: HttpServletRequest, uid: String): Map<String, Any>

    /**
     * 查询登录人的权限
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun queryPop(request: HttpServletRequest): Map<String, Any>

    /**
     * 查询权限ByUid
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    abstract fun queryPopByUid(uid: String): Map<String, Any>


    //修改权限
    @Throws(Exception::class)
    abstract fun editPopByUid(request: HttpServletRequest, uid: String, pops: Array<String>): Map<String, Any>

    //查询是否有此权限
    @Throws(Exception::class)
    abstract fun hasPop(uid: String, pid: String): Boolean

}