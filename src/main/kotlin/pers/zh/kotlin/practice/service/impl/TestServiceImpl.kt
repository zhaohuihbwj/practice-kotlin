package pers.zh.kotlin.practice.service.impl

import com.github.pagehelper.PageHelper
import com.github.pagehelper.PageInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import pers.zh.kotlin.practice.mapper.TestMapper
import pers.zh.kotlin.practice.service.TestService
import java.util.HashMap

@Service("testService")
class TestServiceImpl : TestService {
    @Autowired
    private val testMapper: TestMapper? = null

    /**
     * 分页查询测试
     * @return
     * @throws Exception
     */
    @Throws(Exception::class)
    override fun queryAll(): Map<String, Any> {
        //返回值
        val result = HashMap<String, Any>()

        //分页
        PageHelper.startPage<Any>(1, 2)
        val mapList = testMapper!!.queryAll()

        //总数
        val page = PageInfo<Map<String, Any>>(mapList)
        val count = page.total

        result.put("data", mapList)
        result.put("count", count)

        return result
    }
}