package pers.zh.kotlin.practice.service.impl

import com.github.pagehelper.PageHelper
import com.github.pagehelper.PageInfo
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import pers.zh.kotlin.practice.exception.NoPermissioneException
import pers.zh.kotlin.practice.exception.SqlException
import pers.zh.kotlin.practice.mapper.AnnouncementMapper
import pers.zh.kotlin.practice.mapper.UserMapper
import pers.zh.kotlin.practice.service.AnnouncementService
import pers.zh.kotlin.practice.service.LogService
import pers.zh.kotlin.practice.util.MyUtil
import java.sql.SQLException
import java.util.HashMap
import javax.servlet.http.HttpServletRequest


@Service("announcementService")
class AnnouncementServiceImpl : AnnouncementService{
    @Autowired
    private val announcementMapper: AnnouncementMapper? = null

    @Autowired
    private val userMapper: UserMapper? = null

    @Autowired
    private val logService: LogService? = null


    @Throws(Exception::class)
    override fun queryGongGaoPage(request: HttpServletRequest,
                                  _page: String, _pageSize: String, bt: String, cnm: String, vdt: String): Map<String, Any> {
        // 定义返回值
        val result = HashMap<String, Any>()

        val uid = request.session.getAttribute("uid").toString()

        //分页
        PageHelper.startPage<Any>(Integer.valueOf(_page)!!, Integer.valueOf(_pageSize)!!)
        val mapList = announcementMapper!!.queryGongGaoPage(uid, cnm, bt, vdt)
        result.put("data", mapList)
        //总数
        val page = PageInfo<Map<String, Any>>(mapList)
        result.put("count", page.total)

        return result
    }


    @Transactional
    override fun doSendGongGao(request: HttpServletRequest, bt: String, nr: String): Map<String, Any> {
        val map = HashMap<String, Any>()

        val nid = MyUtil.newGuid()
        val inId = request.session.getAttribute("uid").toString()
        val inDt = MyUtil.getDatetime()
        val inIp = MyUtil.getIpAddr()
        val del = "1"

        //权限控制
        if ("1" != userMapper!!.queryUserByUid(inId).get("TYPE").toString()) {
            throw NoPermissioneException("无操作权限")
        }
        //发公告
        try {
            if (inIp != null) {
                announcementMapper!!.doSendMes(nid, bt, nr, inId, inDt, inIp, del)
            }
            map.put("result", true)
            map.put("message", "发布成功")
            logService!!.addLog("发布公告", inId + "发布了公告：" + nid)
            return map
        } catch (e: SQLException) {
            throw SqlException("数据超时，请刷新后重试！")
        }

    }


    //根据NID进行删除
    @Transactional
    @Throws(Exception::class)
    override fun delByNid(request: HttpServletRequest, nid: String): Map<String, Any> {
        //		定义返回值
        val map = HashMap<String, Any>()
        val deluid = request.session.getAttribute("uid").toString()
        val delip = MyUtil.getIpAddr()
        val deldt = MyUtil.getDatetime()

        //		判断参数
        if ("1" != userMapper!!.queryUserByUid(deluid).get("TYPE").toString()) {
            throw NoPermissioneException("无操作权限")
        }
        //		删除操作
        if (announcementMapper!!.updateDel(nid, deluid, deldt, delip!!)!! > 0) {
            map.put("result", true)
            map.put("message", "删除成功！")
            logService!!.addLog("删除公告", deluid + "删除了公告：" + nid)
            return map
        } else {
            throw SqlException("数据超时，请刷新后重试！")
        }
    }


    /**
     * 根据NID进行查询
     * 创建人：赵辉
     * 创建时间：2017-05-20
     * @param nid
     * @return
     * @throws Exception
     */
    @Transactional
    override fun doReadGongGaoByNid(nid: String, uid: String): Map<String, Any> {
        //如果是收件人进行查看，并处于未读状态 标记为已读
        try {
            if (announcementMapper!!.queryIfRead(uid, nid) === 0) {
                val rid = MyUtil.newGuid()
                val rdt = MyUtil.getDatetime()
                val rip = MyUtil.getIpAddr()
                if (rip != null) {
                    announcementMapper!!.updateRead(rid, nid, uid, rdt, rip)
                }
            }
            return announcementMapper!!.queryGongGaoByNid(nid)
        } catch (e: SQLException) {
            throw SqlException("数据超时，请刷新后重试！")
        }

    }


    //按钮
    override fun queryButton(request: HttpServletRequest): Map<String, Any> {
        val map = HashMap<String, Any>()

        val uid = request.session.getAttribute("uid").toString()
        try {
            if ("1" == userMapper!!.queryUserByUid(uid).get("TYPE").toString()) {
                map.put("result", true)
            } else {
                map.put("result", false)
            }
            return map
        } catch (e: Exception) {
            throw SqlException("数据超时，请刷新后重试！")
        }

    }
}