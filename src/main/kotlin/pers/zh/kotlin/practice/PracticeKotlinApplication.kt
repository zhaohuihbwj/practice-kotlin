package pers.zh.kotlin.practice

import org.mybatis.spring.annotation.MapperScan
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.web.servlet.ServletComponentScan

@SpringBootApplication
@ServletComponentScan
@MapperScan("pers.zh.kotlin.*.mapper")
class PracticeKotlinApplication

fun main(args: Array<String>) {
    SpringApplication.run(PracticeKotlinApplication::class.java, *args)
}
